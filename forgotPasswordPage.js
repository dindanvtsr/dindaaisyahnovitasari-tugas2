import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Alert,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
export default function ForgotPasswordPage({navigation}) {
  const [email, onChangeEmail] = React.useState('');

  return (
    <View style={styles.container}>
      <Text style={styles.back}>
        <Ionicon
          name="arrow-back"
          size={40}
          color={'#5B5B5B'}
          onPress={() => navigation.navigate('LoginPage')}
        />
      </Text>
      <View style={styles.content}>
        <ScrollView style={styles.scrollView}>
          <Image style={styles.logo} source={require('./assets/Logo.png')} />
          <Text style={styles.textLogo}>Reset your password</Text>
          <TextInput
            style={styles.input}
            onChangeText={onChangeEmail}
            value={email}
            placeholder="Email"
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => Alert.alert('Button pressed')}>
            <Text style={styles.buttonTitle}>Request Reset</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
  },
  content: {
    width: '100%',
    padding: 45,
  },
  logo: {
    alignSelf: 'center',
    marginBottom: 17,
  },
  textLogo: {
    fontSize: 18,
    marginBottom: 33,
    textAlign: 'center',
  },
  input: {
    width: '100%',
    marginBottom: 17,
    backgroundColor: '#fff',
    borderRadius: 7,
    alignItems: 'center',
    color: '#959595',
    paddingLeft: 17,
    paddingTop: 14,
    paddingBottom: 14,
  },
  button: {
    backgroundColor: '#2E3283',
    width: '100%',
    marginTop: 20,
    borderRadius: 5,
    marginBottom: 40,
  },
  buttonTitle: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 12,
    padding: 14,
  },
  back: {
    position: 'absolute',
    top: 0,
    paddingLeft: 13,
    paddingTop: 35,
  },
});
