import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Alert,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

export default function RegisterPage({navigation}) {
  const [name, onChangeName] = React.useState('');
  const [email, onChangeEmail] = React.useState('');
  const [phone, onChangePhone] = React.useState('');
  const [password, onChangePassword] = React.useState('');

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <ScrollView>
          <Image style={styles.logo} source={require('./assets/Logo.png')} />
          <Text style={styles.textLogo}>Create an account</Text>
          <TextInput
            style={styles.input}
            onChangeText={onChangeName}
            value={name}
            placeholder="Name"
          />
          <TextInput
            style={styles.input}
            onChangeText={onChangeEmail}
            value={email}
            placeholder="Email"
          />
          <TextInput
            style={styles.input}
            onChangeText={onChangePhone}
            value={phone}
            keyboardType="numeric"
            placeholder="Phone"
          />
          <TextInput
            style={styles.input}
            onChangeText={onChangePassword}
            value={password}
            placeholder="Password"
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => Alert.alert('Button pressed')}>
            <Text style={styles.buttonTitle}>Sign up</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>
          <Text>Already have account? </Text>
          <Text
            style={{fontWeight: 'bold'}}
            onPress={() => navigation.navigate('LoginPage')}>
            Log in
          </Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
  },
  content: {
    width: '100%',
    padding: 45,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 20,
    width: '100%',
  },
  footerText: {
    fontSize: 12,
    textAlign: 'center',
  },
  signUp: {
    fontWeight: 'bold',
  },
  logo: {
    alignSelf: 'center',
    marginBottom: 17,
  },
  textLogo: {
    fontSize: 18,
    marginBottom: 33,
    textAlign: 'center',
  },
  input: {
    width: '100%',
    marginBottom: 17,
    backgroundColor: '#fff',
    borderRadius: 7,
    alignItems: 'center',
    paddingLeft: 17,
    paddingTop: 14,
    paddingBottom: 14,
  },
  button: {
    backgroundColor: '#2E3283',
    width: '100%',
    marginTop: 20,
    borderRadius: 5,
    marginBottom: 40,
  },
  buttonTitle: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 12,
    padding: 14,
  },
});
