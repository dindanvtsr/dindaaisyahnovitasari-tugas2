import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Alert,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import TextBox from 'react-native-password-eye';

export default function LoginPage({navigation}) {
  const [username, onChangeUsername] = React.useState('');
  const [password, onChangePassword] = React.useState('');

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <ScrollView style={styles.scrollView}>
          <Image style={styles.logo} source={require('./assets/Logo.png')} />
          <Text style={styles.textLogo}>Please sign in to continue</Text>
          <TextInput
            style={styles.input}
            onChangeText={onChangeUsername}
            value={username}
            placeholder="Username"
          />
          <TextBox
            placeholderTextColor="#959595"
            hintColor="#fff"
            onChangeText={onChangePassword}
            value={password}
            placeholder="Password"
            secureTextEntry={true}
            containerStyles={styles.textBoxContainer}
            eyeColor="#959595"
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('BottomNavbar')}>
            <Text style={styles.buttonTitle}>Sign in</Text>
          </TouchableOpacity>
          <Text
            style={styles.forgotPass}
            onPress={() => navigation.navigate('ForgotPasswordPage')}>
            Forgot Password
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={styles.garis} />
            <View>
              <Text style={styles.loginWith}>Login with</Text>
            </View>
            <View style={styles.garis} />
          </View>
          <View style={styles.loginLogo}>
            <Image source={require('./assets/facebook.png')} />
            <Image
              source={require('./assets/google.png')}
              style={styles.loginGoogle}
            />
          </View>
          <View style={styles.appVersion}>
            <Text>App Version</Text>
            <Text style={styles.numberVersion}>2.8.3</Text>
          </View>
        </ScrollView>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>
          <Text>Don't have account? </Text>
          <Text
            onPress={() => navigation.navigate('RegisterPage')}
            style={{fontWeight: 'bold'}}>
            Sign up
          </Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
  },
  content: {
    width: '100%',
    padding: 45,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 20,
    width: '100%',
  },
  footerText: {
    fontSize: 12,
    textAlign: 'center',
  },
  logo: {
    alignSelf: 'center',
    marginBottom: 17,
  },
  textLogo: {
    fontSize: 18,
    marginBottom: 33,
    textAlign: 'center',
  },
  input: {
    width: '100%',
    marginBottom: 17,
    backgroundColor: '#fff',
    borderRadius: 7,
    alignItems: 'center',
    paddingLeft: 17,
    paddingTop: 14,
    paddingBottom: 14,
  },
  textBoxContainer: {
    width: '100%',
    marginBottom: 17,
    backgroundColor: '#fff',
    borderRadius: 7,
    alignItems: 'center',
    color: '#959595',
    paddingLeft: 10,
    paddingTop: 4,
    paddingBottom: 4,
    paddingRight: 18,
  },
  button: {
    backgroundColor: '#2E3283',
    width: '100%',
    marginTop: 20,
    borderRadius: 5,
  },
  buttonTitle: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 12,
    padding: 14,
  },
  forgotPass: {
    color: '#2E3283',
    paddingTop: 17,
    paddingBottom: 33,
    textAlign: 'center',
  },
  loginWith: {
    color: '#2E3283',
    paddingHorizontal: 18,
  },
  garis: {
    flex: 1,
    height: 1,
    backgroundColor: '#959595',
  },
  loginLogo: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingTop: 28,
  },
  loginGoogle: {
    marginLeft: 45,
    width: 60,
    height: 60,
    marginTop: -5,
  },
  appVersion: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingTop: 33,
    paddingBottom: 30,
  },
  numberVersion: {
    paddingLeft: 41,
  },
});
