import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';

export default function HomePage1() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.content}>
          <View style={styles.imageContainer}>
            <Image
              source={require('./assets/banner.png')}
              style={styles.banner}
            />
          </View>
          <View style={styles.ferry}>
            <View style={styles.logo}>
              <Image source={require('./assets/intl.png')} />
            </View>
            <View style={styles.logo}>
              <Image source={require('./assets/domestic.png')} />
            </View>
            <View style={styles.logo}>
              <Image source={require('./assets/attraction.png')} />
            </View>
            <View style={styles.logo}>
              <Image source={require('./assets/pioneership.png')} />
            </View>
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => Alert.alert('Button pressed')}>
            <Text style={styles.buttonTitle}>More ...</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  imageContainer: {
    flexDirection: 'row',
  },
  banner: {
    resizeMode: 'cover',
    flex: 1,
    aspectRatio: 2.5,
  },
  ferry: {
    paddingTop: 22,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  logo: {
    alignItems: 'center',
    flex: 1,
  },
  button: {
    backgroundColor: '#2E3283',
    borderRadius: 10,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 126,
    marginTop: 370,
    marginBottom: 50,
  },
  buttonTitle: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
