import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginPage from './loginPage';
import RegisterPage from './registerPage';
import BookingPage from './bookingPage';
import HelpPage from './helpPage';
import ProfilePage from './profilePage';
import HomePage1 from './homePage1';
import BottomNavbar from './bottomNavbar';
import ForgotPasswordPage from './forgotPasswordPage';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="LoginPage" component={LoginPage} />
        <Stack.Screen name="RegisterPage" component={RegisterPage} />
        <Stack.Screen name="BottomNavbar" component={BottomNavbar} />
        <Stack.Screen name="BookingPage" component={BookingPage} />
        <Stack.Screen name="HelpPage" component={HelpPage} />
        <Stack.Screen name="ProfilePage" component={ProfilePage} />
        <Stack.Screen name="HomePage1" component={HomePage1} />
        <Stack.Screen
          name="ForgotPasswordPage"
          component={ForgotPasswordPage}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
