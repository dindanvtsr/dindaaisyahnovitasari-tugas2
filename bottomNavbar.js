import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomePage1 from './homePage1';
import BookingPage from './bookingPage';
import HelpPage from './helpPage';
import ProfilePage from './profilePage';

const Tab = createBottomTabNavigator();

export default function BottomNavbar() {
  return (
    <Tab.Navigator screenOptions={{headerShown: false}}>
      <Tab.Screen
        name="HomePage1"
        component={HomePage1}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: () => <Image source={require('./assets/home.png')} />,
        }}
      />
      <Tab.Screen
        name="BookingPage"
        component={BookingPage}
        options={{
          tabBarLabel: 'My Booking',
          tabBarIcon: () => <Image source={require('./assets/booking.png')} />,
        }}
      />
      <Tab.Screen
        name="HelpPage"
        component={HelpPage}
        options={{
          tabBarLabel: 'Help',
          tabBarIcon: () => <Image source={require('./assets/help.png')} />,
        }}
      />
      <Tab.Screen
        name="ProfilePage"
        component={ProfilePage}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: () => <Image source={require('./assets/profile.png')} />,
        }}
      />
    </Tab.Navigator>
  );
}
